package com.ldy.myblog;

import com.ldy.myblog.mapper.AgreeMapper;
import com.ldy.myblog.pojo.AgreeCountOfUser;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@Slf4j
@SpringBootTest
class MyblogApplicationTests {

    @Autowired
    private AgreeMapper agreeMapper;

    @Test
    void contextLoads() {
        AgreeCountOfUser agreeCountOfUser = agreeMapper.selectAgreeOfUser(16,19);
        System.out.println(agreeCountOfUser);
    }

}
