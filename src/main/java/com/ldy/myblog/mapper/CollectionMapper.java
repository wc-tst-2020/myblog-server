package com.ldy.myblog.mapper;


import com.ldy.myblog.pojo.BlogCount;
import com.ldy.myblog.pojo.BlogList;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface CollectionMapper {
    int insertCollection(@Param("blogId") long blogId, @Param("userId") long userId);
    int deleteCollection(@Param("blogId") long blogId, @Param("userId") long userId);
    List<BlogList> selectCollectionByPage(@Param("userId") long userId, @Param("curPage") int curPage, @Param("pageSize") int pageSize);
    List<BlogList> selectCollectionByTitleByPage(@Param("userId") long userId, @Param("title") String title, @Param("curPage") int curPage, @Param("pageSize") int pageSize);
    BlogCount selectCollectionExit(@Param("blogId") long blogId, @Param("userId") long userId);
    BlogCount selectCollectionCount(@Param("userId") long userId);
    BlogCount selectCollectionCountByTitle(@Param("userId") long userId,@Param("title") String title);
}
