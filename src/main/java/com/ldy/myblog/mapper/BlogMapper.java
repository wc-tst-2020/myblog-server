package com.ldy.myblog.mapper;

import com.ldy.myblog.pojo.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Mapper
@Repository
public interface BlogMapper {
    List<BlogList> selectAll();
    List<BlogList> selectBlogByAuthor(long id);
    List<BlogList> selectSelfBlog(long id);
    BlogPo selectBlogById(long id);
    BlogId selectLastBlogIdByAuthor(long id);
    int insertBlog(Blog blog);
    List<BlogList> selectBlogByTitle(String title);
    List<BlogList> selectSelfBlogByTitle(@Param("id") long id,@Param("title") String title);
    int updateBlog(@Param("title") String title,@Param("content") String content,@Param("description") String description,@Param("date") Date date, @Param("id") long id);
    int deleteBlog(long id);
    /* 分页查询 */
    List<BlogList> selectAllByPage(@Param("curPage") int curPage,@Param("pageSize") int pageSize);
    List<BlogList> selectBlogByAuthorByPage(@Param("id") long id,@Param("curPage") int curPage,@Param("pageSize") int pageSize);
    List<BlogList> selectSelfBlogByPage(@Param("id") long id,@Param("curPage") int curPage,@Param("pageSize") int pageSize);
    List<BlogList> selectBlogByTitleByPage(@Param("title") String title,@Param("curPage") int curPage,@Param("pageSize") int pageSize);
    List<BlogList> selectSelfBlogByTitleByPage(@Param("id") long id,@Param("title") String title,@Param("curPage") int curPage,@Param("pageSize") int pageSize);

    /*获取总页数*/
    BlogCount selectAllCount();
    BlogCount selectBlogByAuthorCount(@Param("id") long id);
    BlogCount selectSelfBlogCount(@Param("id") long id);
    BlogCount selectBlogByTitleCount(@Param("title") String title);
    BlogCount selectSelfBlogByTitleCount(@Param("id") long id,@Param("title") String title);
}
