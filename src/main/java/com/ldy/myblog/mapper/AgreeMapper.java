package com.ldy.myblog.mapper;


import com.ldy.myblog.pojo.AgreeCountOfBlog;
import com.ldy.myblog.pojo.AgreeCountOfUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface AgreeMapper {
    public AgreeCountOfBlog selectAgreeOfBlog(@Param("blogId") long blogId);
    public int updateAgreeOfBlog(@Param("blogId") long blogId, @Param("agree") long agree);

    public int insertAgreeOfUser(@Param("userId") long userId, @Param("blogId") long blogId, @Param("agree") int agree);
    public AgreeCountOfUser selectAgreeOfUser(@Param("userId") long userId, @Param("blogId") long blogId);
    public int updateAgreeOfUser(@Param("userId") long userId, @Param("blogId") long blogId, @Param("agree") int agree);
}
