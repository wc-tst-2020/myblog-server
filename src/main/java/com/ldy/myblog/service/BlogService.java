package com.ldy.myblog.service;


import com.ldy.myblog.pojo.*;

import java.util.Date;
import java.util.List;

public interface BlogService {
    public List<BlogList> getAllBlog();
    public List<BlogList> getBlogByAuthor(long id);
    public List<BlogList> getSelfBlog(long id);
    public BlogPo getBlogById(long id);
    public BlogId getLastBlogIdByAuthor(long id);
    public int saveBlog(Blog blog);
    public List<BlogList> getBlogByTitle(String title);
    public List<BlogList> getSelfBlogByTitle(long id,String title);
    public int setBlog(String title,String content, String des,Date date, long id);
    int delBlog(long id);

    /* 分页 */
    public List<BlogList> getAllBlog(int curPage,int pageSize);
    public List<BlogList> getBlogByAuthor(long id,int curPage,int pageSize);
    public List<BlogList> getSelfBlog(long id,int curPage,int pageSize);
    public List<BlogList> getBlogByTitle(String title,int curPage,int pageSize);
    public List<BlogList> getSelfBlogByTitle(long id,String title,int curPage,int pageSize);

    /* 总页数 */
    public BlogCount getAllBlogCount();
    public BlogCount getBlogByAuthorCount(long id);
    public BlogCount getSelfBlogCount(long id);
    public BlogCount getBlogByTitleCount(String title);
    public BlogCount getSelfBlogByTitleCount(long id,String title);
}
