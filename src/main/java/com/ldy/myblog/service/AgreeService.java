package com.ldy.myblog.service;

import com.ldy.myblog.common.dto.AgreeDto;

public interface AgreeService {
    public void updateAgree(long userId, long blogId, int userAgree, long blogAgree);
    public AgreeDto getAgree(long blogId,long userId);
    public AgreeDto setAgree(long blogId,long userId, int agree);
}
