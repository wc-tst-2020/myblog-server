package com.ldy.myblog.service;

import com.ldy.myblog.common.dto.LoginDto;
import com.ldy.myblog.common.dto.SignUpDto;
import com.ldy.myblog.common.lang.Result;
import com.ldy.myblog.pojo.User;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

public interface MyBlogService {
    // 登录
    public Result login(LoginDto loginDto);

    // 注册
    public Result signUp(SignUpDto signUpDto);

    // 退出
    public Result logout();

    // 获取个人信息
    public Result getUserInfo();

    // 修改个人信息
    public Result setUserInfo(String username);

    // 将个人信息封装为map集合
    public Map getMapUser(User user);

    // 修改密码
    public Result setNewPwd(String oldPwd,String newPwd);

    // 上传图片
    public String upLoadImage(MultipartFile file);

    // 上传头像
    public Result uploadHeadImage(MultipartFile file);

    public Result getAllBlog();

    public Result getBlogByAuthor(long id);

    public Result getSelfBlog();

    public Result getBlogById(long id);

    public Result saveBlog(String title,String content,String des,String editor);

    Result getBlogByTitle(String title);

    Result getSelfBlogByTitle(String title);

    Result showBlogById(long id);

    Result updateBlog(String title,String content, String des,long id);

    Result delBlog(long id);

    /* 分页 */
    public Result getAllBlog(int curPage, int pageSize);

    public Result getBlogByAuthor(long id, int curPage, int pageSize);

    public Result getSelfBlog(int curPage, int pageSize);

    Result getBlogByTitle(String title, int curPage, int pageSize);

    Result getSelfBlogByTitle(String title, int curPage, int pageSize);

    /* 总记录数 */
    public long getSizeOfAllBlog();

    public long getSizeOfBlogByAuthor(long id);

    public long getSizeOfSelfBlog();

    long getSizeOfBlogByTitle(String title);

    long getSizeOfSelfBlogByTitle(String title);

    /* 收藏 */
    Result collectionBlog(long blogId);

    Result unCollectionBlog(long blogId);

    Result getCollectionBlog(int curPage,int pageSize);

    Result getCollectionBlogByTitle(String title,int curPage,int pageSize);

    long getSizeOfCollection(long id);

    long getSizeOfCollectionByTitle(long id,String title);

    boolean isExitCollection(long blogId);

    Result getCollectionExit(long blogId);


    /* 点赞/点踩 */
    Result getAgree(long blogId);

    Result setAgree(long blogId,int agree);
}
