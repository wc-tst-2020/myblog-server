package com.ldy.myblog.service;

import com.ldy.myblog.mapper.BlogMapper;
import com.ldy.myblog.pojo.*;
import com.ldy.myblog.utils.DelTagsUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class BlogServiceImpl implements BlogService{
    @Autowired
    private BlogMapper blogMapper;

    @Autowired
    private DelTagsUtil delTagsUtil;

    @Override
    public List<BlogList> getAllBlog() {
        List<BlogList> result = blogMapper.selectAll();
        for (BlogList blogList : result) {
            blogList.setContent(delTagsUtil.getTextFromHtml(blogList.getContent()));
        }
        return result;
    }

    @Override
    public List<BlogList> getBlogByAuthor(long id) {
        List<BlogList> result = blogMapper.selectBlogByAuthor(id);
        for (BlogList blogList : result) {
            blogList.setContent(delTagsUtil.getTextFromHtml(blogList.getContent()));
        }
        return result;
    }

    @Override
    public List<BlogList> getSelfBlog(long id) {
        List<BlogList> result = blogMapper.selectSelfBlog(id);
        for (BlogList blogList : result) {
            blogList.setContent(delTagsUtil.getTextFromHtml(blogList.getContent()));
        }
        return result;
    }

    @Override
    public BlogPo getBlogById(long id) {
        return blogMapper.selectBlogById(id);
    }

    @Override
    public BlogId getLastBlogIdByAuthor(long id) {
        return blogMapper.selectLastBlogIdByAuthor(id);
    }

    @Transactional(rollbackFor = {RuntimeException.class,Error.class})
    @Override
    public int saveBlog(Blog blog) {
        return blogMapper.insertBlog(blog);
    }

    @Override
    public List<BlogList> getBlogByTitle(String title) {
        List<BlogList> result =  blogMapper.selectBlogByTitle(title);
        for (BlogList blogList : result) {
            blogList.setContent(delTagsUtil.getTextFromHtml(blogList.getContent()));
        }
        return result;
    }

    @Override
    public List<BlogList> getSelfBlogByTitle(long id, String title) {
        List<BlogList> result =  blogMapper.selectSelfBlogByTitle(id,title);
        for (BlogList blogList : result) {
            blogList.setContent(delTagsUtil.getTextFromHtml(blogList.getContent()));
        }
        return result;
    }

    @Transactional(rollbackFor = {RuntimeException.class,Error.class})
    @Override
    public int setBlog(String title,String content, String des,Date date, long id) {
        return blogMapper.updateBlog(title,content,des,date,id);
    }

    @Transactional(rollbackFor = {RuntimeException.class,Error.class})
    @Override
    public int delBlog(long id){
        return blogMapper.deleteBlog(id);
    }

    /* 分页 */

    @Override
    public List<BlogList> getAllBlog(int curPage, int pageSize) {
        curPage = (curPage-1)*pageSize;
        List<BlogList> result = blogMapper.selectAllByPage(curPage,pageSize);
        for (BlogList blogList : result) {
            blogList.setContent(delTagsUtil.getTextFromHtml(blogList.getContent()));
        }
        return result;
    }

    @Override
    public List<BlogList> getBlogByAuthor(long id, int curPage, int pageSize) {
        curPage = (curPage-1)*pageSize;
        List<BlogList> result = blogMapper.selectBlogByAuthorByPage(id,curPage,pageSize);
        for (BlogList blogList : result) {
            blogList.setContent(delTagsUtil.getTextFromHtml(blogList.getContent()));
        }
        return result;
    }

    @Override
    public List<BlogList> getSelfBlog(long id, int curPage, int pageSize) {
        curPage = (curPage-1)*pageSize;
        List<BlogList> result = blogMapper.selectSelfBlogByPage(id,curPage,pageSize);
        for (BlogList blogList : result) {
            blogList.setContent(delTagsUtil.getTextFromHtml(blogList.getContent()));
        }
        return result;
    }

    @Override
    public List<BlogList> getBlogByTitle(String title, int curPage, int pageSize) {
        curPage = (curPage-1)*pageSize;
        List<BlogList> result = blogMapper.selectBlogByTitleByPage(title,curPage,pageSize);
        for (BlogList blogList : result) {
            blogList.setContent(delTagsUtil.getTextFromHtml(blogList.getContent()));
        }
        return result;
    }

    @Override
    public List<BlogList> getSelfBlogByTitle(long id, String title, int curPage, int pageSize) {
        curPage = (curPage-1)*pageSize;
        List<BlogList> result =   blogMapper.selectSelfBlogByTitleByPage(id,title,curPage,pageSize);
        for (BlogList blogList : result) {
            blogList.setContent(delTagsUtil.getTextFromHtml(blogList.getContent()));
        }
        return result;
    }

    /* 总页数 */

    @Override
    public BlogCount getAllBlogCount() {
        return blogMapper.selectAllCount();
    }

    @Override
    public BlogCount getBlogByAuthorCount(long id) {
        return blogMapper.selectBlogByAuthorCount(id);
    }

    @Override
    public BlogCount getSelfBlogCount(long id) {
        return blogMapper.selectSelfBlogCount(id);
    }

    @Override
    public BlogCount getBlogByTitleCount(String title) {
        return blogMapper.selectBlogByTitleCount(title);
    }

    @Override
    public BlogCount getSelfBlogByTitleCount(long id, String title) {
        return blogMapper.selectSelfBlogByTitleCount(id,title);
    }
}
