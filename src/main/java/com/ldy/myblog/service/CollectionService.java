package com.ldy.myblog.service;



import com.ldy.myblog.pojo.BlogCount;
import com.ldy.myblog.pojo.BlogList;

import java.util.List;

public interface CollectionService {
    int addCollection(long blogId,long userId);
    int removeCollection(long blogId,long userId);
    List<BlogList> getCollectionByPage(long userId, int curPage, int pageSize);
    List<BlogList> getCollectionByTitleByPage(long userId,String title,int curPage,int pageSize);
    BlogCount selectCollectionExit(long blogId, long userId);
    BlogCount getSizeOfCollection(long userId);
    BlogCount getSizeOfCollectionByTitle(long userId,String title);
}
