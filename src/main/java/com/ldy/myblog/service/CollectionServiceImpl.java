package com.ldy.myblog.service;

import com.ldy.myblog.mapper.CollectionMapper;
import com.ldy.myblog.pojo.BlogCount;
import com.ldy.myblog.pojo.BlogList;
import com.ldy.myblog.utils.DelTagsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CollectionServiceImpl implements CollectionService{
    @Autowired
    private CollectionMapper collectionMapper;

    @Autowired
    private DelTagsUtil delTagsUtil;

    @Override
    public int addCollection(long blogId, long userId) {
        return collectionMapper.insertCollection(blogId,userId);
    }

    @Override
    public int removeCollection(long blogId, long userId) {
        return collectionMapper.deleteCollection(blogId,userId);
    }

    @Override
    public List<BlogList> getCollectionByPage(long userId, int curPage, int pageSize) {
        curPage = (curPage-1)*pageSize;
        List<BlogList> result = collectionMapper.selectCollectionByPage(userId,curPage,pageSize);
        for (BlogList blogList : result) {
            blogList.setContent(delTagsUtil.getTextFromHtml(blogList.getContent()));
        }
        return result;
    }

    @Override
    public List<BlogList> getCollectionByTitleByPage(long userId, String title, int curPage, int pageSize) {
        curPage = (curPage-1)*pageSize;
        List<BlogList> result = collectionMapper.selectCollectionByTitleByPage(userId,title,curPage,pageSize);
        for (BlogList blogList : result) {
            blogList.setContent(delTagsUtil.getTextFromHtml(blogList.getContent()));
        }
        return result;
    }

    @Override
    public BlogCount selectCollectionExit(long blogId, long userId) {
        return collectionMapper.selectCollectionExit(blogId,userId);
    }

    @Override
    public BlogCount getSizeOfCollection(long userId) {
        return collectionMapper.selectCollectionCount(userId);
    }

    @Override
    public BlogCount getSizeOfCollectionByTitle(long userId, String title) {
        return collectionMapper.selectCollectionCountByTitle(userId,title);
    }
}
