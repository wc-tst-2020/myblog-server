package com.ldy.myblog.service;

import com.ldy.myblog.cache.BlogCache;
import com.ldy.myblog.cache.BlogListCache;
import com.ldy.myblog.cache.UserCache;
import com.ldy.myblog.common.dto.*;
import com.ldy.myblog.common.lang.Result;
import com.ldy.myblog.pojo.Blog;
import com.ldy.myblog.pojo.BlogPo;
import com.ldy.myblog.pojo.User;
import com.ldy.myblog.utils.Constant;
import com.ldy.myblog.utils.FileUtils;
import com.ldy.myblog.utils.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class MyBlogServiceImpl implements MyBlogService {
    @Autowired
    private UserService userService;

    @Autowired
    private BlogService blogService;

    @Autowired
    private CollectionService collectionService;

    @Autowired
    private AgreeService agreeService;

    @Autowired
    private UserCache userCache;

    @Autowired
    private BlogCache blogCache;

    @Autowired
    private BlogListCache blogListCache;

    @Autowired
    private FileUtils fileUtils;

    @Autowired
    private Constant constant; // 存放常量的工具类

    @Override
    public Result login(LoginDto loginDto) {
        Subject subject = SecurityUtils.getSubject();
        String password = new Md5Hash(loginDto.getPassword(),loginDto.getEmail(),3).toString(); // 盐加密：1、密码，2、盐，3、加密次数
        try {
            subject.login(new UsernamePasswordToken(loginDto.getEmail(),password));
        } catch (UnknownAccountException | IncorrectCredentialsException e) {
            return Result.getResult(400,e.getMessage(),loginDto);
        }
        User userInfo = (User) subject.getPrincipal();
        User user = new User(userInfo);
        // 设置最近登录时间
        user.setLast_login(new Date());
        userService.setUserLastLogin(user.getUserid(),user.getLast_login());
        // 将 user 对象放入 redis 缓存
        userCache.setUserIntoCache(user.getEmail(),user);
        // 构造返回数据
        return Result.succ("登录成功！欢迎回来，"+user.getUsername(),new UserInfoDto(user,SecurityUtils.getSubject().getSession().getId()));
    }

    @Override
    public Result signUp(SignUpDto signUpDto) {
        if (signUpDto == null || signUpDto.getEmail().isEmpty() || signUpDto.getPassword().isEmpty()) {
            return Result.fail("用户邮箱或密码不能为空");
        }
        if (signUpDto.getUsername().length() > 25){
            return Result.fail("用户昵称过长");
        }
        if(signUpDto.getPassword().length() < 4){
            return Result.fail("用户密码过短");
        }
        if (signUpDto.getPassword().length() > 25){
            return Result.fail("用户密码过长");
        }
        userService.saveUser(signUpDto);
        return Result.succ("注册成功",signUpDto);
    }

    @Override
    public Result logout() {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        userCache.delUserFromCache(user.getEmail()); // 清除用户数据缓存
        SecurityUtils.getSubject().logout();
        return Result.succ("退出登录成功");
    }

    @Override
    public Result getUserInfo() {
        User userInfo  = (User) SecurityUtils.getSubject().getPrincipal();
        if (userInfo == null) {
            return Result.fail("用户数据异常");
        }
        // 从缓存冲获取用户数据
        User user = userCache.getUserFromCache(userInfo.getUserid(),userInfo.getEmail());
        return Result.succ("操作成功",new UserInfoDto(user));
    }

    @Override
    public Result setUserInfo(String username) {
        User userInfo = (User) SecurityUtils.getSubject().getPrincipal();
        if (userInfo == null || username == null) {
            return Result.fail("用户数据异常");
        }
        // 更新数据库中的用户数据
        int result = userService.setUserName(userInfo.getUserid(),username);
        // 更新缓存中的用户数据
        User user = userCache.getUserFromCache(userInfo.getUserid(),userInfo.getEmail());
        // 维护缓存与数据库的数据一致性
        userCache.delUserFromCache(userInfo.getEmail());
        blogCache.delSelfBlogCache(userInfo.getUserid());
        blogListCache.delBlogListFromCache(userInfo.getUserid());
        if (result > 0) {
            user.setUsername(username);
            return Result.succ("更新用户数据成功",new UserInfoDto(user));
        } else {
            return Result.fail("更新用户数据失败");
        }
    }

    @Deprecated
    @Override
    public Map getMapUser(User user) {
        Map<String,Object> map = new HashMap<>();
        map.put("email",user.getEmail());
        map.put("username",user.getUsername());
        map.put("role",user.getEmail());
        map.put("status",user.getStatus());
        map.put("created",user.getCreated());
        map.put("last_login",user.getLast_login());
        map.put("head_image",user.getHead_image());
        return map;
    }

    @Override
    public Result setNewPwd(String oldPwd,String newPwd){
        User userInfo = (User) SecurityUtils.getSubject().getPrincipal();
        User user = userCache.getUserFromCache(userInfo.getUserid(),userInfo.getEmail());
        String oldPassword = new Md5Hash(oldPwd,user.getEmail(),3).toString(); // 盐加密：1、密码，2、盐，3、加密次数
        String newPassword = new Md5Hash(newPwd,user.getEmail(),3).toString(); // 盐加密：1、密码，2、盐，3、加密次数
        if (user.getPassword().equals(oldPassword)) {
            // 设置新密码
            user.setPassword(newPassword);
            // 将新密码写入数据库
            userService.setUserPassword(user.getUserid(),newPassword);
            // 维护缓存与数据库的数据一致性
            userCache.delUserFromCache(userInfo.getEmail());
            return Result.succ("设置新密码成功");
        } else {
            return Result.fail("旧密码错误");
        }
    }

    /**
     * 用户上传头像
     */
    @Override
    public Result uploadHeadImage(MultipartFile file) {
        // 校验图片格式
        if (!imageTypeRight(file)) return Result.fail("图片格式不正确","/images/default.jpg");
        // 获取上传文件后的路径
        String path = upLoadImage(file);
        User userInfo = (User) SecurityUtils.getSubject().getPrincipal();
        User user = userCache.getUserFromCache(userInfo.getUserid(),userInfo.getEmail());
        // 删除之前的头像(如果是默认头像不删除)
        String image = user.getHead_image();
        if (!image.equals("/images/default.jpg")) {
            if (!fileUtils.del(image.substring(path.lastIndexOf("/") + 1))) {
                log.info("修改头像时, 原来的头像删除失败");
            } else {
                log.info("修改头像时, 原来的头像删除成功");
            }
        }
        // 更新用户头像
        userService.setUserHeadImage(user.getUserid(),path);
        // 维护缓存与数据库的数据一致性
        userCache.delUserFromCache(userInfo.getEmail());
        blogCache.delSelfBlogCache(userInfo.getUserid());
        blogListCache.delBlogListFromCache(userInfo.getUserid());
        return Result.succ("上传头像成功",path);
    }

    /**
     * 文本编辑器上传图片
     * @param file
     * @return
     */
    @Override
    public String upLoadImage(MultipartFile file) {
        if (!imageTypeRight(file)) {
            return null;
        }
        String originalFilename = file.getOriginalFilename();
        String fileSuffix = originalFilename.substring(originalFilename.lastIndexOf(".") + 1).toLowerCase();
        // 只有当满足图片格式时才进来，重新赋图片名，防止出现名称重复的情况
        String newFileName = UUID.randomUUID().toString().replaceAll("-", "") + "." + fileSuffix;
        // 该方法返回的为当前项目的工作目录，即在哪个地方启动的java线程
        File fileTransfer = new File(fileUtils.getPath(), newFileName);
        try {
            file.transferTo(fileTransfer);
            log.info("图片上传: " + fileTransfer.getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 将图片相对路径返回给前端
        return "http://"+constant.getHost()+":"+constant.getPort()+"/blogimage/"+ newFileName;
    }

    /**
     * 验证图片的格式
     *
     * @param file 图片
     * @return
     */
    private boolean imageTypeRight(MultipartFile file) {
        // 首先校验图片格式
        List<String> imageType = new ArrayList<>();
        imageType.add("jpg");
        imageType.add("jpeg");
        imageType.add("png");
        imageType.add("bmp");
        imageType.add("gif");
        // 获取文件名，带后缀
        String originalFilename = file.getOriginalFilename();
        // 获取文件的后缀格式
        String fileSuffix = originalFilename.substring(originalFilename.lastIndexOf(".") + 1).toLowerCase();  //不带 .
        if (!imageType.contains(fileSuffix)) return false;
        return true;
    }

    @Override
    public Result getAllBlog(){
        return Result.succ("查找成功",blogService.getAllBlog());
    }

    @Override
    public Result getBlogByAuthor(long id) {;
        return Result.succ("查找成功",blogService.getBlogByAuthor(id));
    }

    @Override
    public Result getSelfBlog() {
        User userinfo = (User) SecurityUtils.getSubject().getPrincipal();
        return Result.succ("查找成功",blogService.getBlogByAuthor(userinfo.getUserid()));
    }

    @Override
    public Result getBlogById(long id) {
        // 先从缓存中获取博客信息
        return Result.succ("查找成功",blogCache.getBlogFromCache(id));
    }

    @Override
    public Result saveBlog(String title,String content,String des,String editor){
        if(title == null || content == null || des == null || editor == null){
            return Result.fail("数据错误");
        }
        if (title.isEmpty() || content.isEmpty()){
            return Result.fail("标题或正文不能为空");
        }
        if (!constant.getEditors().contains(editor)){
            return Result.fail("编辑器不存在");
        }
        if (editor.equals("editor.md") && des.isEmpty()) {
            return Result.fail("数据错误");
        }
        User userinfo = (User) SecurityUtils.getSubject().getPrincipal();
        User user = userCache.getUserFromCache(userinfo.getUserid(),userinfo.getEmail());
        Blog blog = new Blog(title,content,0,new Date(),userinfo.getUserid(),1,des,editor);
        int result = blogService.saveBlog(blog);
        if (result <= 0) {
            return Result.fail("博客保存不成功");
        }
        // 获取该用户最新发布的博客id
        long blogId = blogService.getLastBlogIdByAuthor(user.getUserid()).getBlogid();
        BlogPo blogPo = new BlogPo(title,content,blogId,new Date(),userinfo.getUserid(),user.getUsername(),user.getHead_image(),1,des,editor);
        BlogShowDto blogShowDto = new BlogShowDto(blogPo,user.getUserid());
        // 将博客详情信息放入缓存
        blogCache.setBlogInCache(blogId, blogPo);
        // 删除博客页面缓存
        blogListCache.delBlogListFromCache(userinfo.getUserid());
        return Result.succ("博客提交成功",blogShowDto);
    }

    @Override
    public Result getBlogByTitle(String title) {
        return Result.succ("查找成功",blogService.getBlogByTitle(title));
    }

    @Override
    public Result getSelfBlogByTitle(String title) {
        User userinfo = (User) SecurityUtils.getSubject().getPrincipal();
        return Result.succ("查找成功",blogService.getSelfBlogByTitle(userinfo.getUserid(),title));
    }

    @Override
    public Result updateBlog(String title,String content,String des,long id) {
        // 判断用户修改的数据是否合法
        if(title == null || content == null || des == null){
            return Result.fail("数据错误");
        }
        if (title.isEmpty() || content.isEmpty()){
            return Result.fail("标题或正文不能为空");
        }
        // 判断修改博客的用户与博客的作者是不是同一个用户
        BlogPo blogPo = blogCache.getBlogFromCache(id);
        User userInfo = (User) SecurityUtils.getSubject().getPrincipal();
        if (blogPo != null && blogPo.getUserid() != userInfo.getUserid()) {
            return Result.fail("不能删除别人的博客");
        }
        Date lastPost = new Date();
        int result = blogService.setBlog(title,content,des,lastPost,id);
        if (result <= 0) {
            return Result.fail("更新失败");
        }
        // 删除博客页面缓存
        blogListCache.delBlogListFromCache(userInfo.getUserid());
        // 删除博客缓存
        blogCache.delBlogFromCache(id);
        return Result.succ("更新成功",id);
    }

    @Override
    public Result delBlog(long id){
        // 判断删除博客的用户与博客的作者是不是同一个用户
        BlogPo blogPo = blogCache.getBlogFromCache(id);
        User userInfo = (User) SecurityUtils.getSubject().getPrincipal();
        if (blogPo != null && blogPo.getUserid() != userInfo.getUserid()) {
            return Result.fail("不能删除别人的博客");
        }
        int result = blogService.delBlog(id);
        // 同时删除缓存中的博客详情
        blogCache.delBlogFromCache(id);
        // 删除博客页面缓存
        blogListCache.delBlogListFromCache(userInfo.getUserid());
        if (result <= 0) {
            return Result.fail("删除失败");
        }
        return Result.succ("删除成功");
    }

    @Override
    public Result showBlogById(long id) {
        BlogShowDto blogShowDto = new BlogShowDto();
        // 从缓存中获取博客信息
        BlogPo blogPo = blogCache.getBlogFromCache(id);
        blogShowDto.setBlogDto(blogPo);
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        blogShowDto.setId(user.getUserid());
        return Result.succ("查找成功",blogShowDto);
    }


    /* 分页 */

    @Override
    public Result getAllBlog(int curPage, int pageSize) {
        ShowBlogList showBlogList = new ShowBlogList();
        showBlogList.setBlogList(blogListCache.getAllBlogFromCache(curPage,pageSize));
        showBlogList.setCount(blogListCache.getSizeOfAllBlogFromCache());
        showBlogList.setCurr(curPage);
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        if(user == null){
            // 因为数据库中绝对不会有负数的用户id，因此可以使用负数的用户id来表示用户还未登录的状态，以此来兼容前后端分离版本
            showBlogList.setId(-5L);
        } else {
            showBlogList.setId(user.getUserid());
        }
        showBlogList.setType("all");
        showBlogList.setTitle("");
        return Result.succ("查找成功",showBlogList);
    }

    @Override
    public Result getBlogByAuthor(long id, int curPage, int pageSize) {
        return Result.succ("查找成功",blogListCache.getBlogByAuthorFromCache(id, curPage, pageSize));
    }

    @Override
    public Result getSelfBlog(int curPage, int pageSize) {
        User userinfo = (User) SecurityUtils.getSubject().getPrincipal();
        ShowBlogList showBlogList = new ShowBlogList();
        showBlogList.setBlogList(blogListCache.getBlogByAuthorFromCache(userinfo.getUserid(),curPage,pageSize));
        showBlogList.setCount(blogListCache.getSizeOfBlogByAuthorFromCache(userinfo.getUserid()));
        showBlogList.setCurr(curPage);
        showBlogList.setId(userinfo.getUserid());
        showBlogList.setType("self");
        showBlogList.setTitle("");
        return Result.succ("查找成功",showBlogList);
    }

    @Override
    public Result getBlogByTitle(String title, int curPage, int pageSize) {
        if (title == null || title.isEmpty()) {
            return Result.fail("关键字不能为空");
        }
        User userinfo = (User) SecurityUtils.getSubject().getPrincipal();
        // 兼容前后端分离版本，允许未登录的用户查阅博客列表
        long id = -5L;
        if(userinfo != null){
            id = userinfo.getUserid();
        }
        ShowBlogList showBlogList = new ShowBlogList();
        showBlogList.setBlogList(blogService.getBlogByTitle(title,curPage,pageSize));
        showBlogList.setCount(getSizeOfBlogByTitle(title));
        showBlogList.setCurr(curPage);
        showBlogList.setId(id);
        showBlogList.setType("title-all");
        showBlogList.setTitle(title);
        return Result.succ("查找成功",showBlogList);
    }

    @Override
    public Result getSelfBlogByTitle(String title, int curPage, int pageSize) {
        if (title == null || title.isEmpty()) {
            return Result.fail("关键字不能为空");
        }
        User userinfo = (User) SecurityUtils.getSubject().getPrincipal();
        ShowBlogList showBlogList = new ShowBlogList();
        showBlogList.setBlogList(blogService.getSelfBlogByTitle(userinfo.getUserid(),title,curPage,pageSize));
        showBlogList.setCount(getSizeOfSelfBlogByTitle(title));
        showBlogList.setCurr(curPage);
        showBlogList.setId(userinfo.getUserid());
        showBlogList.setType("title-self");
        showBlogList.setTitle(title);
        return Result.succ("查找成功",showBlogList);
    }


    /* 总记录数 */

    @Override
    public long getSizeOfAllBlog() {
        return blogService.getAllBlogCount().getCount();
    }

    @Override
    public long getSizeOfBlogByAuthor(long id) {
        return blogService.getBlogByAuthorCount(id).getCount();
    }

    @Override
    public long getSizeOfSelfBlog() {
        User userinfo = (User) SecurityUtils.getSubject().getPrincipal();
        return blogService.getBlogByAuthorCount(userinfo.getUserid()).getCount();
    }

    @Override
    public long getSizeOfBlogByTitle(String title) {
        if (title == null || title.isEmpty()) {
            return 0L;
        }
        return blogService.getBlogByTitleCount(title).getCount();
    }

    @Override
    public long getSizeOfSelfBlogByTitle(String title) {
        if (title == null || title.isEmpty()) {
            return 0L;
        }
        User userinfo = (User) SecurityUtils.getSubject().getPrincipal();
        return blogService.getSelfBlogByTitleCount(userinfo.getUserid(),title).getCount();
    }

    /* 收藏 */

    @Override
    public Result collectionBlog(long blogId) {
        User userinfo = (User) SecurityUtils.getSubject().getPrincipal();
        int rows = collectionService.addCollection(blogId,userinfo.getUserid());
        // 删除博客收藏页缓存
        blogListCache.delCollectionBlogListFromCache(userinfo.getUserid());
        if (rows > 0) {
            return Result.succ("添加收藏成功");
        }
        return Result.fail("添加收藏失败");
    }

    @Override
    public Result unCollectionBlog(long blogId) {
        User userinfo = (User) SecurityUtils.getSubject().getPrincipal();
        int rows = collectionService.removeCollection(blogId,userinfo.getUserid());
        // 删除博客收藏页缓存
        blogListCache.delCollectionBlogListFromCache(userinfo.getUserid());
        if (rows > 0) {
            return Result.succ("移出收藏成功");
        }
        return Result.fail("移出收藏失败");
    }

    @Override
    public Result getCollectionBlog(int curPage, int pageSize) {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        ShowBlogList showBlogList = new ShowBlogList();
        showBlogList.setBlogList(blogListCache.getCollectionBlogFromCache(user.getUserid(),curPage,pageSize));
        showBlogList.setCount(blogListCache.getSizeOfCollectionFromCache(user.getUserid()));
        showBlogList.setCurr(curPage);
        showBlogList.setId(user.getUserid());
        showBlogList.setType("collection-all");
        showBlogList.setTitle("");
        return Result.succ("查找成功",showBlogList);
    }

    @Override
    public Result getCollectionBlogByTitle(String title, int curPage, int pageSize) {
        if (title == null || title.isEmpty()) {
            return Result.fail("关键字不能为空");
        }
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        ShowBlogList showBlogList = new ShowBlogList();
        showBlogList.setBlogList(collectionService.getCollectionByTitleByPage(user.getUserid(),title,curPage,pageSize));
        showBlogList.setCount(getSizeOfCollectionByTitle(user.getUserid(),title));
        showBlogList.setCurr(curPage);
        showBlogList.setId(user.getUserid());
        showBlogList.setType("collection-title");
        showBlogList.setTitle(title);
        return Result.succ("查找成功",showBlogList);
    }

    @Override
    public long getSizeOfCollection(long id) {
        return collectionService.getSizeOfCollection(id).getCount();
    }

    @Override
    public long getSizeOfCollectionByTitle(long id, String title) {
        if (title == null || title.isEmpty()) {
            return 0L;
        }
        return collectionService.getSizeOfCollectionByTitle(id,title).getCount();
    }

    @Override
    public boolean isExitCollection(long blogId) {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        if(user == null){
            return false;
        }
        return collectionService.selectCollectionExit(blogId, user.getUserid()).getCount() > 0;
    }

    @Override
    public Result getCollectionExit(long blogId) {
        return Result.succ("查询成功",isExitCollection(blogId));
    }

    /* 点赞/点踩 */
    @Override
    public Result getAgree(long blogId) {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        long userId = -5;
        if(user != null){
            userId = user.getUserid();
        }
        return Result.succ("查询成功",agreeService.getAgree(blogId,userId));
    }

    @Override
    public Result setAgree(long blogId,int agree) {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        return Result.succ("查询成功",agreeService.setAgree(blogId,user.getUserid(),agree));
    }
}
