package com.ldy.myblog.service;

import com.ldy.myblog.cache.AgreeCache;
import com.ldy.myblog.common.dto.AgreeDto;
import com.ldy.myblog.mapper.AgreeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AgreeServiceImpl implements AgreeService{
    @Autowired
    private AgreeMapper agreeMapper;

    @Autowired
    private AgreeCache agreeCache;

    @Autowired
    @Qualifier(value = "sqlThreadPool")
    private ThreadPoolTaskExecutor sqlThreadPool;

    @Override
    @Transactional(rollbackFor = {RuntimeException.class,Error.class})
    public void updateAgree(long userId, long blogId, int userAgree, long blogAgree){
        agreeMapper.updateAgreeOfBlog(blogId,blogAgree);
        if (agreeMapper.selectAgreeOfUser(userId, blogId) == null) { // 添加记录
            agreeMapper.insertAgreeOfUser(userId, blogId, userAgree);
        } else {
            agreeMapper.updateAgreeOfUser(userId, blogId, userAgree);
        }
    }

    @Override
    public AgreeDto getAgree(long blogId, long userId) {
        AgreeDto agreeDto = new AgreeDto();
        agreeDto.setAgreeOfBlog(agreeCache.getAgreeOfBlogFromCache(blogId));
        agreeDto.setAgreeOfUser(agreeCache.getAgreeOfUserFromCache(userId,blogId));
        return agreeDto;
    }

    @Override
    @Transactional(rollbackFor = {RuntimeException.class,Error.class})
    public AgreeDto setAgree(long blogId, long userId, int agree) {
        // 获取旧数据
        long agreeOfBlog = agreeCache.getAgreeOfBlogFromCache(blogId); //保证缓存里有数据
        // 检查用户是否登录
        if(userId <= 0){
            // 如果用户没有登录，那么只返回博客的被点赞数
            return new AgreeDto(agreeOfBlog,0);
        }
        // 获取用户点赞的旧数据
        int oldAgreeOfUser = agreeCache.getAgreeOfUserFromCache(userId,blogId);

        // 对传入的数据进行处理，防止非法数据
        if (agree > 0) {
            agree = 1;
        } else if (agree < 0) {
            agree = -1;
        } else {
            agree = 0;
        }

        // 确定 点赞/取消点赞/改变点赞 等操作
        int condition = agree * oldAgreeOfUser;
        if (condition < 0) { // 改变点赞/踩
            agreeOfBlog = agreeCache.incrAgreeOfBlog(blogId,-2L * oldAgreeOfUser);
        } else if (condition == 0) {
            if (agree == 0) { // 取消点赞/踩
                agreeOfBlog = agreeCache.incrAgreeOfBlog(blogId,-1L * oldAgreeOfUser);
            } else { // 正常点赞/踩
                agreeOfBlog = agreeCache.incrAgreeOfBlog(blogId,agree);
            }
        } else { // 异常操作，直接跳过后面的处理
            return new AgreeDto(agreeOfBlog,agree);
        }

        // 更新缓存
        agreeCache.setAgreeOfUserIntoCache(userId,blogId,agree);

        // 更新数据库表
        final long blogAgree = agreeOfBlog;
        final int userAgree = agree;
        sqlThreadPool.execute(()->{
            updateAgree(userId,blogId,userAgree,blogAgree);
        });

        return new AgreeDto(agreeOfBlog,agree);
    }
}
