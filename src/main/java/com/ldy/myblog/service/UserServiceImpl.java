package com.ldy.myblog.service;

import com.ldy.myblog.common.dto.SignUpDto;
import com.ldy.myblog.mapper.UserMapper;
import com.ldy.myblog.pojo.User;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public User getUserById(long id) {
        return userMapper.selectUserById(id);
    }

    @Override
    public User getUserByEmail(String email) {
        if (email == null) {
            return null;
        }
        return userMapper.selectUserByEmail(email);
    }

    @Transactional(rollbackFor = {RuntimeException.class,Error.class})
    @Override
    public int setUserLastLogin(long id, Date lastLogin) {
        if (lastLogin == null) {
            return 0;
        }
        return userMapper.updateLastLogin(id,lastLogin);
    }

    @Transactional(rollbackFor = {RuntimeException.class,Error.class})
    @Override
    public int saveUser(SignUpDto signUpDto) throws RuntimeException {
        String password = new Md5Hash(signUpDto.getPassword(),signUpDto.getEmail(),3).toString(); // 盐加密：1、密码，2、盐，3、加密次数
        User user = new User(0,signUpDto.getEmail(),signUpDto.getUsername(),password,"user",1,new Date(),null,null);
        if (userMapper.selectUserByEmail(user.getEmail()) != null) {
            throw new RuntimeException("该用户已经存在");
        }
        return userMapper.insertUser(user);
    }

    @Transactional(rollbackFor = {RuntimeException.class,Error.class})
    @Override
    public int setUserName(long id,String name) {
        return userMapper.updateUserName(id,name);
    }

    @Transactional(rollbackFor = {RuntimeException.class,Error.class})
    @Override
    public int setUserPassword(long id, String password) {
        return userMapper.updateUserPassword(id,password);
    }

    @Transactional(rollbackFor = {RuntimeException.class,Error.class})
    @Override
    public int setUserHeadImage(long id, String headImage) {
        return userMapper.updateUserHeadImage(id,headImage);
    }
}
