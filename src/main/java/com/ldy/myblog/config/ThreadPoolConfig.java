package com.ldy.myblog.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

@Configuration
@EnableAsync
public class ThreadPoolConfig {

    @Value("${threadPool.sqlThreads}")
    private int sqlThreads;

    @Value("${threadPool.cacheThreads}")
    private int cacheThreads;

    @Bean("sqlThreadPool") // bean的名称，默认为首字母小写的方法名
    public ThreadPoolTaskExecutor sqlThreadPool() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(sqlThreads);
        executor.setMaxPoolSize(sqlThreads);
        executor.setThreadNamePrefix("sqlThreadPool");

        // 线程池对拒绝任务的处理策略
        // CallerRunsPolicy：由调用线程（提交任务的线程）处理该任务
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        // 初始化
        executor.initialize();
        return executor;
    }

    @Bean("cacheThreadPool") // bean的名称，默认为首字母小写的方法名
    public ThreadPoolTaskExecutor cacheThreadPool() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(cacheThreads);
        executor.setMaxPoolSize(cacheThreads);
        executor.setThreadNamePrefix("cacheThreads");

        // 线程池对拒绝任务的处理策略
        // CallerRunsPolicy：由调用线程（提交任务的线程）处理该任务
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        // 初始化
        executor.initialize();
        return executor;
    }
}
