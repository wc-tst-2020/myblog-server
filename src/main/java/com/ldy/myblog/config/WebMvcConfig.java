package com.ldy.myblog.config;

import com.ldy.myblog.utils.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.io.File;


@Configuration
public class WebMvcConfig extends WebMvcConfigurationSupport {
    @Autowired
    private FileUtils fileUtils;

    @Value("${upload.path}")
    private String uploadFolder;

    static final String ORIGINS[] = new String[] { "GET", "POST", "PUT", "DELETE" };

    /**
     * 解决请跨域
     * */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowCredentials(true)
                .allowedMethods(ORIGINS)
                .allowedHeaders("*")
                .maxAge(3600);
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // 加入头像文件夹映射 可通过 localhost:7188/headimage/....   访问到指定位置的图片
        registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
        registry.addResourceHandler("/blogimage/**").addResourceLocations("file:" + uploadFolder);
        super.addResourceHandlers(registry);
    }
}
