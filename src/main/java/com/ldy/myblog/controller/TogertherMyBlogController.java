package com.ldy.myblog.controller;

import com.ldy.myblog.common.dto.LoginDto;
import com.ldy.myblog.common.dto.SignUpDto;
import com.ldy.myblog.common.lang.Result;
import com.ldy.myblog.pojo.BlogPo;
import com.ldy.myblog.pojo.User;
import com.ldy.myblog.service.MyBlogService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Slf4j
@Controller
public class TogertherMyBlogController {
    @Autowired
    private MyBlogService myBlogService;

    @GetMapping("/")
    public String to(Model model){
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        if (user != null) {  // 用户已经登录过了
            return "redirect:/tomain";
        } else {
            return "redirect:/index";
        }
    }

    @RequiresAuthentication
    @GetMapping("/tomain")
    public String toMain(Model model){
        Result result = myBlogService.getUserInfo();
        model.addAttribute("result", result);
        if (result.getCode() != 200) {
            return "exception";
        }
        return "main";
    }

    @GetMapping("/index")
    public String index(){
        return "index";
    }

    @Value("${session.timeout}")
    private int timeout;

    @PostMapping("/login")
    public String login(@RequestParam(name="email") String email, @RequestParam(name="password") String password, HttpServletRequest request, HttpServletResponse response, Model model){
        // 检查用户是否已经登录
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        if (user != null) {  // 用户已经登录过了
            return "redirect:/tomain";
        }
        Result result = myBlogService.login(new LoginDto(email,password));
        model.addAttribute("result",result);
        if (result.getCode() == 200) {
            // 保存sessionId到cookie中
            HttpSession session = request.getSession();
            Cookie cookieSID = new Cookie("JSESSIONID",session.getId());
            cookieSID.setHttpOnly(true); // 设置成仅允许http方位，提高安全性
            cookieSID.setMaxAge(timeout * 24 * 60 * 60); // 七天过期
            cookieSID.setPath("/");
            response.addCookie(cookieSID);
            return "redirect:/tomain";
        } else {
            return "index";
        }
    }

    @GetMapping("/tosignup")
    public String toSignUp(){
        return "signup";
    }

    @PostMapping("/signup")
    public String signUp(@RequestParam(name="email") String email, @RequestParam(name = "name") String name, @RequestParam(name="password") String password,Model model) {
        Result result = myBlogService.signUp(new SignUpDto(email,name,password));
        model.addAttribute("result",result);
        if (result.getCode() != 200) {
            return "signup";
        }
        return "index";
    }

    @RequiresAuthentication
    @GetMapping("/logout")
    public String logout(Model model){
        Result result = myBlogService.logout();
        model.addAttribute("result",result);
        return "index";
    }

    @RequiresAuthentication
    @GetMapping("/getuserinfo")
    public String getUserinfo(Model model){
        Result result = myBlogService.getUserInfo();
        model.addAttribute("result",result);
        if (result.getCode() != 200) {
            return "exception";
        }
        return "userinfo";
    }

    @RequiresAuthentication
    @PostMapping("/setuserinfo")
    public String setUserinfo(@RequestParam(name = "username") String username, Model model) {
        Result result = myBlogService.setUserInfo(username);
        model.addAttribute("result",result);
        if (result.getCode() != 200) {
            return "exception";
        }
        return "userinfo";
    }

    @RequiresAuthentication
    @GetMapping("/tosetpwd")
    public String toSetPwd(){
        return "setpwd";
    }

    @RequiresAuthentication
    @PostMapping("/setpwd")
    public String setPwd(@RequestParam(name = "oldpassword") String oldPassword,@RequestParam(name = "newpassword") String newPassword,Model model){
        Result result = myBlogService.setNewPwd(oldPassword,newPassword);
        model.addAttribute("result",result);
        if (result.getCode() != 200) {
            return "exception";
        }
        return "setpwd";
    }

    @RequiresAuthentication
    @GetMapping("/toMdEdit")
    public String toMdEdit(){
        return "edit";
    }

    @RequiresAuthentication
    @GetMapping("/toWangEdit")
    public String toWangEdit(){
        return "wangEdit";
    }

    @RequiresAuthentication
    @GetMapping("/toblog/{curPage}/{pageSize}")
    public String toBlog(@PathVariable int curPage,@PathVariable int pageSize,Model model){
        Result result = myBlogService.getAllBlog(curPage,pageSize);
        model.addAttribute("result", result);
        if (result.getCode() != 200) {
            return "exception";
        }
        return "blog";
    }

    @RequiresAuthentication
    @GetMapping("/getSelfBlog/{curPage}/{pageSize}")
    public String getSelfBlog(@PathVariable int curPage,@PathVariable int pageSize,Model model) {
        Result result = myBlogService.getSelfBlog(curPage,pageSize);
        model.addAttribute("result", result);
        if (result.getCode() != 200) {
            return "exception";
        }
        return "blog";
    }

    @RequiresAuthentication
    @ResponseBody
    @PostMapping("/uploadHeadImage")
    public Result uploadHeadImage(MultipartFile file){
        return myBlogService.uploadHeadImage(file);
    }

    @RequiresAuthentication
    @ResponseBody
    @PostMapping("/uploadImage")
    public MdResult uploadImage(@RequestParam(value = "editormd-image-file", required = false) MultipartFile file){
        String result = myBlogService.upLoadImage(file);
        if (result == null) {
            return new MdResult(0,"图片格式不正确","/images/default.jpg");
        }
        return new MdResult(1,"上传成功",result);
    }

    //MdResult的返回类型，注意返回的格式success，message，url
    private class MdResult {
        public Integer success;

        public String message;

        public String url;

        public MdResult(Integer success, String message, String url) {
            this.success = success;
            this.message = message;
            this.url = url;
        }
    }


    @RequiresAuthentication
    @PostMapping("/saveMdBlog")
    public String saveMdBlog(@RequestParam("title") String title,@RequestParam("text") String content,@RequestParam("test-editormd-markdown-doc") String des,Model model){
        Result result = myBlogService.saveBlog(title,content,des,"editor.md");
        model.addAttribute("result", result);
        if (result.getCode() != 200) {
            return "exception";
        }
        return "blogshow";
    }

    @RequiresAuthentication
    @PostMapping("/saveWangBlog")
    public String saveWangBlog(@RequestParam("title") String title,@RequestParam("content") String content,Model model){
        Result result = myBlogService.saveBlog(title,content,null,"wangEditor");
        model.addAttribute("result", result);
        if (result.getCode() != 200) {
            return "exception";
        }
        return "blogshow";
    }

    @RequiresAuthentication
    @GetMapping("/toblogshow")
    public String toblogshow(@RequestParam("id") long id,Model model) {
        Result result = myBlogService.getBlogById(id);
        model.addAttribute("result", result);
        if (result.getCode() != 200) {
            return "exception";
        }
        return "blogshow";
    }

    @RequiresAuthentication
    @GetMapping("/toshowblog/{id}")
    public String toshowblog(@PathVariable("id") long id,Model model) {
        Result result = myBlogService.showBlogById(id);
        model.addAttribute("result", result);
        if (result.getCode() != 200) {
            return "exception";
        }
        return "blogshow";
    }

    @RequiresAuthentication
    @PostMapping("/getBlogByTitle/{pageSize}")
    public String getBlogByTitle(@RequestParam("title") String title,@PathVariable int pageSize,Model model) {
        Result result = myBlogService.getBlogByTitle(title,1,pageSize);
        model.addAttribute("result", result);
        if (result.getCode() != 200) {
            return "exception";
        }
        return "blog";
    }

    @RequiresAuthentication
    @GetMapping("/getBlogByTitle/{curPage}/{pageSize}")
    public String getBlogByTitle(@RequestParam("title") String title,@PathVariable int curPage,@PathVariable int pageSize,Model model) {
        Result result = myBlogService.getBlogByTitle(title,curPage,pageSize);
        model.addAttribute("result", result);
        if (result.getCode() != 200) {
            return "exception";
        }
        return "blog";
    }

    @RequiresAuthentication
    @PostMapping("/getSelfBlogByTitle/{pageSize}")
    public String getSelfBlogByTitle(@RequestParam("title") String title,@PathVariable int pageSize,Model model) {
        Result result = myBlogService.getSelfBlogByTitle(title,1,pageSize);
        model.addAttribute("result", result);
        if (result.getCode() != 200) {
            return "exception";
        }
        return "blog";
    }

    @RequiresAuthentication
    @GetMapping("/getSelfBlogByTitle/{curPage}/{pageSize}")
    public String getSelfBlogByTitle(@RequestParam("title") String title,@PathVariable int curPage,@PathVariable int pageSize,Model model) {
        Result result = myBlogService.getSelfBlogByTitle(title,curPage,pageSize);
        model.addAttribute("result", result);
        if (result.getCode() != 200) {
            return "exception";
        }
        return "blog";
    }

    @RequiresAuthentication
    @GetMapping("/welcome")
    public String welcome(){
        return "welcome";
    }

    @RequiresAuthentication
    @GetMapping("/toUpdateBlog/{id}")
    public String toUpdateBlog(@PathVariable("id") long id,Model model){
        Result result = myBlogService.getBlogById(id);
        model.addAttribute("result",result);
        if (result.getCode() != 200) {
            return "exception";
        }
        BlogPo blogPo = (BlogPo) result.getData();
        if (blogPo.getEditor().equals("editor.md")) {
            return "UpdateEdit";
        } else {
            return "UpdateWangEdit";
        }
    }

    @RequiresAuthentication
    @PostMapping("/updateBlog/{id}")
    public String updateBlog(@PathVariable("id") long id,@RequestParam("title") String title,@RequestParam("text") String content,@RequestParam("test-editormd-markdown-doc") String des,Model model){
        Result result = myBlogService.updateBlog(title,content,des,id);
        model.addAttribute("result",result);
        if (result.getCode() != 200) {
            return "exception";
        }
        return "redirect:/toshowblog/"+id;
    }

    @RequiresAuthentication
    @PostMapping("/updateWangBlog/{id}")
    public String updateWangBlog(@PathVariable("id") long id,@RequestParam("title") String title,@RequestParam("text") String content,Model model){
        Result result = myBlogService.updateBlog(title,content,null,id);
        model.addAttribute("result",result);
        if (result.getCode() != 200) {
            return "exception";
        }
        return "redirect:/toshowblog/"+id;
    }

    @RequiresAuthentication
    @GetMapping("/deleteBlog/{id}")
    public String deleteBlog(@PathVariable("id") long id,Model model) {
        Result result = myBlogService.delBlog(id);
        model.addAttribute("result", result);
        if (result.getCode() != 200) {
            return "exception";
        }
        return "redirect:/toblog/1/20";
    }

    @RequiresAuthentication
    @GetMapping("/deleteSelfBlog/{id}")
    public String deleteSelfBlog(@PathVariable("id") long id,Model model) {
        Result result = myBlogService.delBlog(id);
        model.addAttribute("result", result);
        if (result.getCode() != 200) {
            return "exception";
        }
        return "redirect:/getSelfBlog/1/20";
    }

    /* 收藏 */

    @RequiresAuthentication
    @ResponseBody
    @PostMapping("/blog/collection")
    public Result collectionBlog(long blogId){
        return myBlogService.collectionBlog(blogId);
    }

    @RequiresAuthentication
    @ResponseBody
    @PostMapping("/blog/unCollection")
    public Result unCollectionBlog(long blogId){
        return myBlogService.unCollectionBlog(blogId);
    }

    @RequiresAuthentication
    @GetMapping("/blog/list/collection/{curPage}/{pageSize}")
    public String showCollection(@PathVariable int curPage,@PathVariable int pageSize,Model model){
        Result result = myBlogService.getCollectionBlog(curPage,pageSize);
        model.addAttribute("result", result);
        if (result.getCode() != 200) {
            return "exception";
        }
        return "blog";
    }

    @RequiresAuthentication
    @GetMapping("/blog/list/title/collection/{curPage}/{pageSize}")
    public String showCollectionByTitle(@RequestParam("title") String title,@PathVariable int curPage,@PathVariable int pageSize,Model model){
        Result result = myBlogService.getCollectionBlogByTitle(title,curPage,pageSize);
        model.addAttribute("result", result);
        if (result.getCode() != 200) {
            return "exception";
        }
        return "blog";
    }

    @RequiresAuthentication
    @PostMapping("/blog/list/title/collection/{pageSize}")
    public String getCollectionByTitle(@RequestParam("title") String title,@PathVariable int pageSize,Model model) {
        Result result = myBlogService.getCollectionBlogByTitle(title,1,pageSize);
        model.addAttribute("result", result);
        if (result.getCode() != 200) {
            return "exception";
        }
        return "blog";
    }

    @RequiresAuthentication
    @ResponseBody
    @PostMapping("/blog/isCollection")
    public Result isBlogCollection(long blogId){
        return myBlogService.getCollectionExit(blogId);
    }

    @RequiresAuthentication
    @ResponseBody
    @PostMapping("/blog/getAgree")
    public Result getAgree(long blogId){
        return myBlogService.getAgree(blogId);
    }

    @RequiresAuthentication
    @ResponseBody
    @PostMapping("/blog/setAgree")
    public Result setAgree(long blogId,int agree){
        return myBlogService.setAgree(blogId,agree);
    }
}
