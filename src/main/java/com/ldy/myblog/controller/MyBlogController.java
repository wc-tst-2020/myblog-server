package com.ldy.myblog.controller;

import com.ldy.myblog.common.dto.*;
import com.ldy.myblog.common.lang.Result;
import com.ldy.myblog.pojo.User;
import com.ldy.myblog.service.MyBlogService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/server")
public class MyBlogController {
    @Autowired
    private MyBlogService myBlogService;

    @RequiresAuthentication
    @GetMapping("/isAuth")
    public Result isAuth(){
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        if (user == null) {
            return Result.fail("您尚未登录或获得授权");
        }
        return Result.succ("已经登录",user.getUserid());
    }

    @RequiresAuthentication
    @GetMapping("/getUserinfo")
    public Result getUserInfo (){
        return myBlogService.getUserInfo();
    }

    @PostMapping("/login")
    public Result login(@Validated @RequestBody LoginDto loginDto){
        return myBlogService.login(loginDto);
    }

    @PostMapping("/signup")
    public Result signUp(@Validated @RequestBody SignUpDto signUpDto) {
        return myBlogService.signUp(signUpDto);
    }

    @RequiresAuthentication
    @PostMapping("/logout")
    public Result logout(){
        return myBlogService.logout();
    }

    @RequiresAuthentication
    @PostMapping("/setUserName")
    public Result setUserName(@Validated @RequestBody SetUserNameDto setUserNameDto){
        return myBlogService.setUserInfo(setUserNameDto.getUsername());
    }

    @RequiresAuthentication
    @PostMapping("/setPassword")
    public Result setPassword(@Validated @RequestBody SetPasswordDto setPasswordDto){
        return myBlogService.setNewPwd(setPasswordDto.getOldPassword(),setPasswordDto.getNewPassword());
    }

    // 阅读博客不需要验证
    @PostMapping("/getAllBlog")
    public Result getAllBlogByPage(@Validated @RequestBody GetBlogDto getBlogDto){
        return myBlogService.getAllBlog(getBlogDto.getCurPage(),getBlogDto.getPageSize());
    }

    // 但是阅读个人博客与个人收藏博客需要验证
    @RequiresAuthentication
    @PostMapping("/getSelfBlog")
    public Result getSelfBlogByPage(@Validated @RequestBody GetBlogDto getBlogDto){
        return myBlogService.getSelfBlog(getBlogDto.getCurPage(),getBlogDto.getPageSize());
    }

    @RequiresAuthentication
    @PostMapping("/getCollectionBlog")
    public Result getCollectionBlogByPage(@Validated @RequestBody GetBlogDto getBlogDto){
        return myBlogService.getCollectionBlog(getBlogDto.getCurPage(),getBlogDto.getPageSize());
    }

    @PostMapping("/getAllBlogByTitle")
    public Result getAllBlogByTitle(@Validated @RequestBody GetBlogByTitleDto getBlogByTitleDto){
        return myBlogService.getBlogByTitle(getBlogByTitleDto.getTitle(),getBlogByTitleDto.getCurPage(),getBlogByTitleDto.getPageSize());
    }

    @RequiresAuthentication
    @PostMapping("/getSelfBlogByTitle")
    public Result getSelfBlogByTitle(@Validated @RequestBody GetBlogByTitleDto getBlogByTitleDto){
        return myBlogService.getSelfBlogByTitle(getBlogByTitleDto.getTitle(),getBlogByTitleDto.getCurPage(),getBlogByTitleDto.getPageSize());
    }

    @RequiresAuthentication
    @PostMapping("/getCollectionBlogByTitle")
    public Result getCollectionBlogByTitle(@Validated @RequestBody GetBlogByTitleDto getBlogByTitleDto){
        return myBlogService.getCollectionBlogByTitle(getBlogByTitleDto.getTitle(),getBlogByTitleDto.getCurPage(),getBlogByTitleDto.getPageSize());
    }

    @PostMapping("/getBlogById")
    public Result getBlogById(@Validated @RequestBody BlogIdDto blogIdDto){
        return myBlogService.getBlogById(blogIdDto.getBlogid());
    }


    /* 收藏 */

    @RequiresAuthentication
    @ResponseBody
    @PostMapping("/isCollection")
    public Result isBlogCollection(@Validated @RequestBody BlogIdDto blogIdDto){
        return myBlogService.getCollectionExit(blogIdDto.getBlogid());
    }

    @RequiresAuthentication
    @ResponseBody
    @PostMapping("/collectionBlog")
    public Result collectionBlog(@Validated @RequestBody BlogIdDto blogIdDto){
        return myBlogService.collectionBlog(blogIdDto.getBlogid());
    }

    @RequiresAuthentication
    @ResponseBody
    @PostMapping("/unCollectionBlog")
    public Result unCollectionBlog(@Validated @RequestBody BlogIdDto blogIdDto){
        return myBlogService.unCollectionBlog(blogIdDto.getBlogid());
    }

    /* 点赞 */

    //@RequiresAuthentication 获取点赞数据不需要经过验证
    @ResponseBody
    @PostMapping("/getAgree")
    public Result getAgree(@Validated @RequestBody BlogIdDto blogIdDto){
        return myBlogService.getAgree(blogIdDto.getBlogid());
    }

    @RequiresAuthentication
    @ResponseBody
    @PostMapping("/setAgree")
    public Result setAgree(@Validated @RequestBody SetAgreeDto setAgreeDto){
        return myBlogService.setAgree(setAgreeDto.getBlogId(),setAgreeDto.getAgree());
    }

    /* 删除博客需要验证用户身份 */
    @RequiresAuthentication
    @PostMapping("/deleteBlog")
    public Result deleteBlog(@Validated @RequestBody BlogIdDto blogIdDto){
        return myBlogService.delBlog(blogIdDto.getBlogid());
    }

    // 保存博客
    @RequiresAuthentication
    @PostMapping("/saveMdBlog")
    public Result saveMdBlog(@Validated @RequestBody SaveBlogDto saveBlogDto){
        return myBlogService.saveBlog(saveBlogDto.getTitle(),saveBlogDto.getContent(),saveBlogDto.getDescription(),"editor.md");
    }

    @RequiresAuthentication
    @PostMapping("/saveWangBlog")
    public Result saveWangBlog(@Validated @RequestBody SaveBlogDto saveBlogDto){
        return myBlogService.saveBlog(saveBlogDto.getTitle(),saveBlogDto.getContent(),saveBlogDto.getDescription(),"wangEditor");
    }

    // 更新博客
    @RequiresAuthentication
    @PostMapping("/updateBlog")
    public Result updateBlog(@Validated @RequestBody UpdateBlogDto updateBlogDto){
        return myBlogService.updateBlog(updateBlogDto.getTitle(),updateBlogDto.getContent(),updateBlogDto.getDescription(),updateBlogDto.getBlogid());
    }

}
