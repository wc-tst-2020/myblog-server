package com.ldy.myblog.cache;

import com.ldy.myblog.pojo.BlogList;
import com.ldy.myblog.service.BlogService;
import com.ldy.myblog.service.CollectionService;
import com.ldy.myblog.utils.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Component
public class BLogListCacheImpl implements BlogListCache{
    @Autowired
    private CollectionService collectionService;

    @Autowired
    private BlogService blogService;

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    @Qualifier(value = "cacheThreadPool")
    private ThreadPoolTaskExecutor cacheThreadPool;

    @Value("${session.timeout}")
    private int cacheTimeout; // 缓存过期时间，单位天

    @Value("${delCacheDelay.blogListCache}")
    private int blogListCacheDelay; // 延时双删的延时时间，单位毫秒

    @Override
    public List<BlogList> getAllBlogFromCache(int curPage, int pageSize) {
        String key = "getAllBlog_on_"+curPage+"_size_"+pageSize;
        List<BlogList> result =  (List<BlogList>) redisUtils.get(key);
        if (result == null) {
            result = blogService.getAllBlog(curPage,pageSize);
            redisUtils.set(key,result);
            redisUtils.setRandomExpire(key,cacheTimeout,cacheTimeout,TimeUnit.DAYS); // 设置缓存过期时间，单位天
            // 记录页面key，方便日后删除
            List<String> allBlogListKey = (List<String>) redisUtils.get("AllBlogListKey");
            if (allBlogListKey == null) {
                allBlogListKey = new ArrayList();
            }
            allBlogListKey.add(key);
            redisUtils.set("AllBlogListKey",allBlogListKey);
        }
        return result;
    }

    @Override
    public List<BlogList> getBlogByAuthorFromCache(long userId, int curPage, int pageSize) {
        String key = "getBlogBy_"+userId+"_on_"+curPage+"_size_"+pageSize;
        List<BlogList> result = (List<BlogList>) redisUtils.get(key);
        if (result == null) {
            result = blogService.getBlogByAuthor(userId,curPage,pageSize);
            redisUtils.set(key,result);
            redisUtils.setRandomExpire(key,cacheTimeout,cacheTimeout,TimeUnit.DAYS); // 设置缓存过期时间，单位天
            // 记录页面key，方便日后删除
            List<String> authorBlogListKey = (List<String>) redisUtils.get("AuthorBlogListKeyOf"+userId);
            if (authorBlogListKey == null) {
                authorBlogListKey = new ArrayList();
            }
            authorBlogListKey.add(key);
            redisUtils.set("AuthorBlogListKeyOf"+userId,authorBlogListKey);
        }
        return result;
    }

    @Override
    public List<BlogList> getCollectionBlogFromCache(long userId, int curPage, int pageSize) {
        String key = "getCollectionBlog_of_"+userId+"_on_"+curPage+"_size_"+pageSize;
        List<BlogList> result =  (List<BlogList>) redisUtils.get(key);
        if (result == null) {
            result = collectionService.getCollectionByPage(userId,curPage,pageSize);
            redisUtils.set(key,result);
            redisUtils.setRandomExpire(key,cacheTimeout,cacheTimeout,TimeUnit.DAYS); // 设置缓存过期时间，单位天
            // 记录页面key，方便日后删除
            List<String> collectionBlogListKey = (List<String>) redisUtils.get("CollectionBlogListKey");
            if (collectionBlogListKey == null) {
                collectionBlogListKey = new ArrayList();
            }
            collectionBlogListKey.add(key);
            redisUtils.set("CollectionBlogListKey",collectionBlogListKey);
            // 记录页面key，方便日后删除
            List<String> selfCollectionBlogListKey = (List<String>) redisUtils.get("SelfCollectionBlogListKeyOf"+userId);
            if (selfCollectionBlogListKey == null) {
                selfCollectionBlogListKey = new ArrayList();
            }
            selfCollectionBlogListKey.add(key);
            redisUtils.set("SelfCollectionBlogListKeyOf"+userId,selfCollectionBlogListKey);
        }
        return result;
    }

    @Override
    public void delBlogListFromCache(long userId) {
        doDelBlogListFromCache(userId);
        // 延时双删
        cacheThreadPool.execute(()->{
            try {
                Thread.sleep(blogListCacheDelay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            doDelBlogListFromCache(userId); // 过一段时间又执行一次删除操作
        });
    }

    private void doDelBlogListFromCache(long userId) {
        // 删除博客主页缓存
        List<String> allBlogListKey = (List<String>) redisUtils.get("AllBlogListKey");
        if (allBlogListKey != null) {
            for (String key : allBlogListKey) {
                redisUtils.del(key);
            }
        }
        // 删除个人博客页缓存
        List<String> authorBlogListKey = (List<String>) redisUtils.get("AuthorBlogListKeyOf"+ userId);
        if (authorBlogListKey != null) {
            for (String key : authorBlogListKey) {
                redisUtils.del(key);
            }
        }
        // 删除所有人的收藏页面缓存
        List<String> collectionBlogListKey = (List<String>) redisUtils.get("CollectionBlogListKey");
        if (collectionBlogListKey != null) {
            for (String key : collectionBlogListKey) {
                redisUtils.del(key);
            }
        }
        // 删除所有人的收藏页面缓存的总记录数
        List<String> allCollectionSize = (List<String>) redisUtils.get("AllCollectionSize");
        if (allCollectionSize != null) {
            for (String key : allCollectionSize) {
                redisUtils.del(key);
            }
        }
        redisUtils.del("AllBlogListKey","AuthorBlogListKeyOf"+ userId,"CollectionBlogListKey","AllCollectionSize","getSizeOfAllBlog","getSizeOfBlogByAuthor_of_"+ userId);
    }

    // 用户更新操作的一致性策略
    @Override
    public void delCollectionBlogListFromCache(long userId) {
        doDelCollectionBlogListFromCache(userId);
        // 延时双删
        cacheThreadPool.execute(()->{
            try {
                Thread.sleep(blogListCacheDelay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            doDelCollectionBlogListFromCache(userId); // 过一段时间又执行一次删除操作
        });
    }

    private void doDelCollectionBlogListFromCache(long userId) {
        // 删除个人收藏页缓存
        List<String> selfCollectionBlogListKey = (List<String>) redisUtils.get("SelfCollectionBlogListKeyOf"+ userId);
        if (selfCollectionBlogListKey != null) {
            for (String key : selfCollectionBlogListKey) {
                redisUtils.del(key);
            }
        }
        redisUtils.del("getSizeOfCollection_of_"+ userId,"SelfCollectionBlogListKeyOf"+ userId);
    }

    @Override
    public long getSizeOfAllBlogFromCache() {
        String key = "getSizeOfAllBlog";
        Integer sizeOfAllBlog = (Integer) redisUtils.get(key);
        Long result = null;
        if (sizeOfAllBlog == null) {
            result = blogService.getAllBlogCount().getCount();
            redisUtils.set(key,result);
        } else {
            result = sizeOfAllBlog.longValue();
        }
        return result;
    }

    @Override
    public long getSizeOfBlogByAuthorFromCache(long id) {
        String key = "getSizeOfBlogByAuthor_of_"+id;
        Integer sizeOfBlogByAuthor = (Integer) redisUtils.get(key);
        Long result = null;
        if (sizeOfBlogByAuthor == null) {
            result = blogService.getBlogByAuthorCount(id).getCount();
            redisUtils.set(key,result);
        } else {
            result = sizeOfBlogByAuthor.longValue();
        }
        return result;
    }

    @Override
    public long getSizeOfCollectionFromCache(long id) {
        String key = "getSizeOfCollection_of_"+id;
        Integer sizeOfAllBlog = (Integer) redisUtils.get(key);
        Long result = null;
        if (sizeOfAllBlog == null) {
            result = collectionService.getSizeOfCollection(id).getCount();
            redisUtils.set(key,result);
            // 记录页面key，方便日后删除
            List<String> allCollectionSize = (List<String>) redisUtils.get("AllCollectionSize");
            if (allCollectionSize == null) {
                allCollectionSize = new ArrayList();
            }
            allCollectionSize.add(key);
            redisUtils.set("AllCollectionSize",allCollectionSize);
        } else {
            result = sizeOfAllBlog.longValue();
        }
        return result;
    }
}
