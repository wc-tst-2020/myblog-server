package com.ldy.myblog.cache;

import com.ldy.myblog.common.dto.SignUpDto;
import com.ldy.myblog.pojo.User;

import java.util.Date;

public interface UserCache {
    void setUserIntoCache(String email,User user);
    User getUserFromCache(long id,String email);
    void delUserFromCache(String email);
}
