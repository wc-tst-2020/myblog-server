package com.ldy.myblog.cache;

public interface AgreeCache {
    long getAgreeOfBlogFromCache(long blogId);
    int getAgreeOfUserFromCache(long userId,long blogId);
    void setAgreeOfUserIntoCache(long userId,long blogId,int agree);
    long incrAgreeOfBlog(long blogId,long incr);
}
