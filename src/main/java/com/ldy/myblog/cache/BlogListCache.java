package com.ldy.myblog.cache;

import com.ldy.myblog.pojo.BlogList;

import java.util.List;

public interface BlogListCache {
    List<BlogList> getAllBlogFromCache(int curPage, int pageSize);
    List<BlogList> getBlogByAuthorFromCache(long userId,int curPage,int pageSize);
    List<BlogList> getCollectionBlogFromCache(long userId,int curPage, int pageSize);
    void delBlogListFromCache(long userId);
    void delCollectionBlogListFromCache(long userId);
    long getSizeOfAllBlogFromCache();
    long getSizeOfBlogByAuthorFromCache(long id);
    long getSizeOfCollectionFromCache(long id);
}
