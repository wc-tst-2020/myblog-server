package com.ldy.myblog.cache;

import com.ldy.myblog.pojo.BlogPo;
import com.ldy.myblog.service.BlogService;
import com.ldy.myblog.utils.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class BlogCacheImpl implements BlogCache{
    @Autowired
    private BlogService blogService;

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    @Qualifier(value = "cacheThreadPool")
    private ThreadPoolTaskExecutor cacheThreadPool;

    @Value("${session.timeout}")
    private int cacheTimeout; // 缓存过期时间，单位天

    @Value("${delCacheDelay.blogCache}")
    private int blogCacheDelay; // 延时双删的延时时间，单位毫秒

    /**
     * 从缓存中获取博客详情，如果缓存中没有，就中数据库中获取
     * @param id
     * @return
     */
    @Override
    public BlogPo getBlogFromCache(long id) {
        BlogPo blogPo = (BlogPo) redisUtils.get("blogPo"+id);
        if (blogPo == null) {
            // 如果缓存中没有博客信息，就从数据库中获取
            blogPo = blogService.getBlogById(id);
            setBlogInCache(id, blogPo);
        }
        return blogPo;
    }

    /**
     * 将博客详情放入缓存中
     * @param id
     * @param blogPo
     */
    @Override
    public void setBlogInCache(long id, BlogPo blogPo) {
        // 把博客信息加入缓存中
        String key = "blogPo" + id;
        redisUtils.set(key, blogPo);
        redisUtils.setRandomExpire(key,cacheTimeout,cacheTimeout,TimeUnit.DAYS); // 设置缓存过期时间，单位天
        // 把博客id按作者id分类存入缓存
        List<String> blogKeyOfUser = (List<String>) redisUtils.get("blogKeyOfUser" + blogPo.getUserid());
        if (blogKeyOfUser == null) {
            blogKeyOfUser = new ArrayList<>();
        }
        blogKeyOfUser.add(key);
        redisUtils.set("blogKeyOfUser" + blogPo.getUserid(), blogKeyOfUser);
    }

    /**
     * 删除缓存中的博客
     * @param id
     */
    @Override
    public void delBlogFromCache(long id) {
        redisUtils.del("blogPo" + id);
        // 延时双删
        cacheThreadPool.execute(()->{
            try {
                Thread.sleep(blogCacheDelay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            redisUtils.del("blogPo" + id);
        });
    }

    /**
     * 删除个人的所有博客缓存
     * @param id
     */
    @Override
    public void delSelfBlogCache(long id) {
        doDelSelfBlogCache(id);
        // 延时双删
        cacheThreadPool.execute(()->{
            try {
                Thread.sleep(blogCacheDelay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            doDelSelfBlogCache(id);
        });
    }

    private void doDelSelfBlogCache(long id) {
        List<String> blogKeyOfUser = (List<String>) redisUtils.get("blogKeyOfUser"+ id);
        if(blogKeyOfUser != null){
            for (String blogKey : blogKeyOfUser) {
                redisUtils.del(blogKey);
            }
        }
        redisUtils.del("blogKeyOfUser"+ id);
    }
}
