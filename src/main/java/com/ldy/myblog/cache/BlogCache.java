package com.ldy.myblog.cache;

import com.ldy.myblog.pojo.BlogPo;

public interface BlogCache {
    BlogPo getBlogFromCache(long id);
    void setBlogInCache(long id, BlogPo blogPo);
    void delBlogFromCache(long id);
    void delSelfBlogCache(long id);
}
