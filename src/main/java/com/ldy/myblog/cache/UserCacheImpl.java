package com.ldy.myblog.cache;

import com.ldy.myblog.pojo.User;
import com.ldy.myblog.service.UserService;
import com.ldy.myblog.utils.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class UserCacheImpl implements UserCache{
    @Autowired
    private UserService userService;

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    @Qualifier(value = "cacheThreadPool")
    private ThreadPoolTaskExecutor cacheThreadPool;

    @Value("${session.timeout}")
    private int cacheTimeout; // 缓存过期时间，单位天

    @Value("${delCacheDelay.userCache}")
    private int userCacheDelay; // 延时双删的延时时间，单位毫秒

    @Override
    public void setUserIntoCache(String email, User user) {
        redisUtils.set(email,user);
        // 设置缓存过期时间,单位：天
        redisUtils.setExpire(user.getEmail(),cacheTimeout, TimeUnit.DAYS);
    }

    @Override
    public User getUserFromCache(long id,String email) {
        User user = (User) redisUtils.get(email);
        if (user == null) { // 如果缓存中没有记录
            user = userService.getUserById(id); // 从数据库中获取
            setUserIntoCache(email,user); // 放入缓存
        }
        return user;
    }

    @Override
    public void delUserFromCache(String email) {
        redisUtils.del(email);
        // 延时双删
        cacheThreadPool.execute(()->{
            try {
                Thread.sleep(userCacheDelay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            redisUtils.del(email);
        });
    }
}
