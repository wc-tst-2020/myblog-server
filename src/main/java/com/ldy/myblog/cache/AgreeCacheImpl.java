package com.ldy.myblog.cache;

import com.ldy.myblog.mapper.AgreeMapper;
import com.ldy.myblog.pojo.AgreeCountOfUser;
import com.ldy.myblog.utils.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class AgreeCacheImpl implements AgreeCache{
    @Autowired
    private AgreeMapper agreeMapper;

    @Autowired
    private RedisUtils redisUtils;

    @Value("${session.timeout}")
    private int cacheTimeout; // 缓存过期时间，单位天

    @Override
    public long getAgreeOfBlogFromCache(long blogId) {
        String key = "agreeOfBlog"+blogId;
        Integer tempAgree = (Integer) redisUtils.get(key);
        Long agreeOfBlog = null;
        if (tempAgree == null){
            agreeOfBlog = agreeMapper.selectAgreeOfBlog(blogId).getAgree();
            redisUtils.set(key,agreeOfBlog);
            // 设置缓存过期时间,单位：天
            redisUtils.setExpire(key,cacheTimeout, TimeUnit.DAYS);
        } else {
            agreeOfBlog = tempAgree.longValue();
        }
        return agreeOfBlog;
    }

    @Override
    public int getAgreeOfUserFromCache(long userId, long blogId) {
        String key = "agreeOfUser"+userId+"in"+blogId;
        Integer agreeOfUser = (Integer) redisUtils.get(key);
        if (agreeOfUser == null){
            if (userId <= 0) {
                agreeOfUser = 0;
            } else {
                AgreeCountOfUser agreeCountOfUser = agreeMapper.selectAgreeOfUser(userId, blogId);
                if (agreeCountOfUser == null) {
                    // 如果用户还没决定是否点赞/踩就先别添加记录，以提高效率与节约磁盘开销
                    // agreeMapper.insertAgreeOfUser(userId, blogId, 0);
                    agreeOfUser = 0;
                } else {
                    agreeOfUser = agreeCountOfUser.getAgree();
                }
            }
            redisUtils.set(key,agreeOfUser);
            // 设置缓存过期时间,单位：天
            redisUtils.setExpire(key,cacheTimeout, TimeUnit.DAYS);
        }
        return  agreeOfUser;
    }

    @Override
    public void setAgreeOfUserIntoCache(long userId, long blogId, int agree) {
        String key = "agreeOfUser"+userId+"in"+blogId;
        redisUtils.set(key,agree);
    }

    @Override
    public long incrAgreeOfBlog(long blogId, long incr) {
        return redisUtils.incrBy("agreeOfBlog"+blogId,incr);
    }
}
