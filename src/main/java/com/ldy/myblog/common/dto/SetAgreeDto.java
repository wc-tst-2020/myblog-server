package com.ldy.myblog.common.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class SetAgreeDto implements Serializable {
    @JSONField(name = "blogId")
    private long blogId;

    @JSONField(name = "agree")
    private int agree;
}
