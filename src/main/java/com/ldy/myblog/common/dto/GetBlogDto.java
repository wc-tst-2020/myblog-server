package com.ldy.myblog.common.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class GetBlogDto implements Serializable {
    @JSONField(name = "curPage")
    private int curPage;

    @JSONField(name = "pageSize")
    private int pageSize;
}
