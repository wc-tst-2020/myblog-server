package com.ldy.myblog.common.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class UpdateBlogDto implements Serializable {
    @JSONField(name = "blogid")
    private long blogid;

    @JSONField(name = "title")
    private String title;

    @JSONField(name = "content")
    private String content;

    @JSONField(name = "description")
    private String description;
}
