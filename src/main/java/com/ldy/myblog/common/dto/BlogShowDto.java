package com.ldy.myblog.common.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.ldy.myblog.pojo.BlogPo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class BlogShowDto implements Serializable {
    @JSONField(name = "BLOGDto")
    private BlogPo blogDto; // 仍然保留blogDto旧名，以减少前端的改动

    @JSONField(name = "USERID")
    private long id;
}
