package com.ldy.myblog.common.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class SaveBlogDto implements Serializable {
    @JSONField(name = "title")
    private String title;

    @JSONField(name = "content")
    private String content;

    @JSONField(name = "description")
    private String description;
}
