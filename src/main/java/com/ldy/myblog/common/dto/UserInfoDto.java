package com.ldy.myblog.common.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.ldy.myblog.pojo.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserInfoDto {
    private Serializable sessionId; // Session id

    private String email; // 用户邮箱地址，相当于用户id

    private String username; // 用户昵称

    private String role; // 用户角色 用于权限控制

    private int status; // 用户状态

    private Date created; // 注册时间

    private Date last_login; // 最后一次登录时间

    private String head_image; // 用户头像

    private void userToUserInfoDto(User user){
        email = user.getEmail();
        username = user.getUsername();
        role = user.getRole();
        status = user.getStatus();
        created = user.getCreated();
        last_login = user.getLast_login();
        head_image = user.getHead_image();
    }

    public UserInfoDto(User user,Serializable sessionId){
        this.sessionId = sessionId;
        userToUserInfoDto(user);

    }

    public UserInfoDto(User user){
        userToUserInfoDto(user);
    }
}
