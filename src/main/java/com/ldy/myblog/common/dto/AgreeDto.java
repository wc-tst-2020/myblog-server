package com.ldy.myblog.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class AgreeDto {
    private long agreeOfBlog;
    private int agreeOfUser;
}
