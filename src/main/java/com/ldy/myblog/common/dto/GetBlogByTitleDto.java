package com.ldy.myblog.common.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class GetBlogByTitleDto implements Serializable {
    @JSONField(name = "title")
    private String title;

    @JSONField(name = "curPage")
    private int curPage;

    @JSONField(name = "pageSize")
    private int pageSize;
}
