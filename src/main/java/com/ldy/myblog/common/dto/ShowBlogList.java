package com.ldy.myblog.common.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.ldy.myblog.pojo.BlogList;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ShowBlogList implements Serializable {
    @JSONField(name = "blogList")
    private List<BlogList> blogList;

    @JSONField(name = "count")
    private long count;

    @JSONField(name = "curr")
    private int curr;

    @JSONField(name = "title")
    private String title;

    @JSONField(name = "id")
    private long id;

    @JSONField(name = "type")
    private String type;
}
