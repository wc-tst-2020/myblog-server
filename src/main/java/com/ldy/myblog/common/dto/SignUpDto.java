package com.ldy.myblog.common.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class SignUpDto implements Serializable {
    @JSONField(name = "email")
    private String email;

    @JSONField(name = "username")
    private String username;

    @JSONField(name = "password")
    private String password;
}
