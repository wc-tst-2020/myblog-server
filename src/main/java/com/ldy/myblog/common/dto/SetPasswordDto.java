package com.ldy.myblog.common.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class SetPasswordDto implements Serializable {
    @JSONField(name = "oldPassword")
    private String oldPassword;

    @JSONField(name = "newPassword")
    private String newPassword;
}
