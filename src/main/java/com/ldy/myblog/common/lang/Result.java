package com.ldy.myblog.common.lang;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Result <T> {
    private int code; // 状态码，200 代表一切正常，非200代表有错误
    private String msg; // 信息
    private T data; // 需要传递的数据

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public T getData() {
        return data;
    }

    // 返回一个结果，参数1：状态码，参数2：信息，参数3：需要传给前端类（什么类型都行）
    public static <T> Result getResult(int code,String msg,T data){
        return new Result(code,msg,data);
    }

    // 返回成功结果
    public static <T> Result succ(String msg,T data){
        return getResult(200,msg,data);
    }

    public static Result succ(String msg){
        return getResult(200,msg,null);
    }

    // 返回失败结果
    public static <T> Result fail(String msg,T data){
        return getResult(400,msg,data);
    }

    public static Result fail(String msg){
        return getResult(400,msg,null);
    }

    // 返回异常信息
    public static Result exception(int code,String msg){
        return getResult(code,msg,null);
    }
}
