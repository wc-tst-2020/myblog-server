package com.ldy.myblog.common.handler;

import com.ldy.myblog.common.lang.Result;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.ShiroException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.sql.SQLException;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {
    //@ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = RuntimeException.class)
    public Result handler(RuntimeException e){
        log.debug("运行时异常：{}",e);
        return Result.exception(500,e.getMessage());
    }

    //@ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = SQLException.class)
    public Result handler(SQLException e){
        log.debug("sql语句异常：{}",e);
        return Result.exception(400,e.getMessage());
    }

    // 需要让前端知道用户还没登录（还没有权限）
    //@ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = ShiroException.class)
    public Result handler(ShiroException e){
        log.debug("shiro异常：{}",e);
        return Result.exception(403,"您尚未登录或获得授权");
    }

}
