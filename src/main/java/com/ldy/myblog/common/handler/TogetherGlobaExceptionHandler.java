package com.ldy.myblog.common.handler;

import com.ldy.myblog.common.lang.Result;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.ShiroException;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.sql.SQLException;

@Slf4j
//@ControllerAdvice
public class TogetherGlobaExceptionHandler {
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = RuntimeException.class)
    public String handler(RuntimeException e, Model model){
        log.debug("运行时异常：{}",e);
        model.addAttribute("result",Result.exception(500,e.getMessage()));
        return "exception";
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = SQLException.class)
    public String handler(SQLException e,Model model){
        log.debug("sql语句异常：{}",e);
        model.addAttribute("result",Result.exception(400,e.getMessage()));
        return "exception";
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = ShiroException.class)
    public String handler(ShiroException e,Model model){
        log.debug("shiro异常：{}",e);
        model.addAttribute("result",Result.exception(403,"您尚未登录或获得授权"));
        return "exception";
    }

}

