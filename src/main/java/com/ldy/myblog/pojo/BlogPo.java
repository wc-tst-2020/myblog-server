package com.ldy.myblog.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class BlogPo implements Serializable {
    @JSONField(name = "TITLE")
    private String title; // 博客标题

    @JSONField(name = "CONTENT")
    private String content; // 博客正文

    @JSONField(name = "BLOGID")
    private long blogid; // 博客id

    @JSONField(name = "DATE")
    private Date date; // 提交或修改的日期

    @JSONField(name = "USERID")
    private long userid; // 作者id

    @JSONField(name = "USERNAME")
    private String username; // 作者昵称

    @JSONField(name = "HEAD_IMAGE")
    private String head_image; // 作者头像

    @JSONField(name = "STATUS")
    private int status; // 博客状态

    @JSONField(name = "DESCRIPTION")
    private String description; // 博客描述

    @JSONField(name = "TITLE")
    private String editor; // 编写博客时用的编辑器
}
