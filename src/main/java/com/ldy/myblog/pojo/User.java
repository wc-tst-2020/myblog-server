package com.ldy.myblog.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class User implements Serializable {
    @JSONField(name = "USERID")
    private long userid; // 用户id

    @JSONField(name = "EMAIL")
    private String email; // 用户邮箱地址，相当于用户id

    @JSONField(name = "USERNAME")
    private String username; // 用户昵称

    @JSONField(name = "PASSWORD")
    private String password; // 用户密码

    @JSONField(name = "ROLE")
    private String role; // 用户角色 用于权限控制

    @JSONField(name = "STATUS")
    private int status; // 用户状态

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    @JSONField(name = "CREATED")
    private Date created; // 注册时间

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    @JSONField(name = "LAST LOGIN")
    private Date last_login; // 最后一次登录时间

    @JSONField(name = "HEAD IMAGE")
    private String head_image; // 用户头像

    public User(User user) {
        this.setUserid(user.getUserid());
        this.setEmail(user.getEmail());
        this.setUsername(user.getUsername());
        this.setPassword(user.getPassword());
        this.setRole(user.getRole());
        this.setStatus(user.getStatus());
        this.setCreated(user.getCreated());
        this.setLast_login(user.getLast_login());
        this.setHead_image(user.getHead_image());
    }
}
