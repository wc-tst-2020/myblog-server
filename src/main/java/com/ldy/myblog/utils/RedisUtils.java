package com.ldy.myblog.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
@Slf4j
public class RedisUtils {

    @Autowired
    private RedisTemplate<String,Object> redisTemplate;

    /**
     * @param key
     * @return 获得值
     * redis有五种数据类型 opsForValue表示是操作字符串类型
     */
    public Object get(String key){
        return  key == null ? null : redisTemplate.opsForValue().get(key);
    }

    /**
     * 放入缓存
     * @param key
     * @param value
     * @return
     */
    //本来只可以放入string类型，但是我们配置了自动序列化所以这儿可以传入object
    public boolean set(String key,Object value){
        try{
            redisTemplate.opsForValue().set(key,value);
            return true;
        }catch (Exception e){
            log.error("redis set value exception:{}",e);
            return false;
        }
    }

    /**
     *
     * @param keys 删除key 可变参数
     */
    public void del(String ...keys){
        if(keys!=null&&keys.length>0) {
            redisTemplate.delete((Collection<String>) CollectionUtils.arrayToList(keys));
        }
    }

    /**
     * redis 的模糊删除
     * @param key
     */
    public void fuzzyDel(String key){
        if (key != null && !key.isEmpty()) {
            Set<String> keys = redisTemplate.keys(key);
            if (keys != null && !keys.isEmpty()) {
                redisTemplate.delete(key);
            }
        }
    }

    /**
     * 设置缓存过期时间
     * @param key
     * @param timeout
     * @param unit
     */
    public void setExpire(String key,long timeout,TimeUnit unit){
        redisTemplate.expire(key,timeout,unit);
    }

    /**
     * 设置随机的缓存时间
     * @param key
     * @param timeout 基础过期时间
     * @param maxRandomTimeout 基础过期时间需要加上的最长过期时间
     * @param unit 时间单位
     */
    public void setRandomExpire(String key, long timeout, long maxRandomTimeout, TimeUnit unit){
        long randomTimeout = (long) (Math.random() % (maxRandomTimeout+1));
        redisTemplate.expire(key,timeout+randomTimeout,unit);
    }

    /**
     * 自增
     * @param key
     * @param increment
     * @return
     */
    public Long incrBy(String key,long increment){
        return  redisTemplate.opsForValue().increment(key,increment);
    }
}
