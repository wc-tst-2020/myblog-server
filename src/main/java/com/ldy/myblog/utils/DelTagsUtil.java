package com.ldy.myblog.utils;

import org.springframework.stereotype.Component;

@Component
public class DelTagsUtil {
    /**
     * 去除html代码中含有的标签
     * @param htmlStr
     * @return
     */
    public String delHtmlTags(String htmlStr) {
        //定义script的正则表达式，去除js可以防止注入
        String scriptRegex="<script[^>]*?>[\\s\\S]*?<\\/script>";
        //定义style的正则表达式，去除style样式，防止css代码过多时只截取到css样式代码
        String styleRegex="<style[^>]*?>[\\s\\S]*?<\\/style>";
        // 定义 img 的正则表达式，替换img标签为"[查看图片]"字样
        String imgRegex="<img[^>]*?>";
        //定义 pre 的正则表达式，替换代码块为 “[查看代码]”字样
        String preRegex="<pre[^>]*?>[\\s\\S]*?<\\/pre>";
        //定义HTML标签的正则表达式，去除标签，只提取文字内容
        String htmlRegex="<[^>]+>";
        //定义空格,回车,换行符,制表符
        String spaceRegex = "\\s*|\t|\r|\n";

        // 过滤script标签
        htmlStr = htmlStr.replaceAll(scriptRegex, "");
        // 过滤style标签
        htmlStr = htmlStr.replaceAll(styleRegex, "");
        // 过滤img标签
        htmlStr = htmlStr.replaceAll(imgRegex,"[查看图片]");
        // 过滤pre标签
        htmlStr = htmlStr.replaceAll(preRegex,"[查看代码]");
        // 过滤html标签
        htmlStr = htmlStr.replaceAll(htmlRegex, "");
        // 过滤空格等
        htmlStr = htmlStr.replaceAll(spaceRegex, "");
        return htmlStr.trim(); // 返回文本字符串
    }
    /**
     * 获取HTML代码里的内容
     * @param htmlStr
     * @return
     */
    public String getTextFromHtml(String htmlStr){
        //去除html标签
        htmlStr = delHtmlTags(htmlStr);
//        //去除空格" "
//        htmlStr = htmlStr+"...";
        return htmlStr;
    }
}
