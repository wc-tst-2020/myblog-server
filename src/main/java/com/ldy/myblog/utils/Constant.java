package com.ldy.myblog.utils;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Component
@Data
public class Constant {
    @Value("${myblog.host}")
    private String host;

    @Value("${server.port}")
    private int port;

    @Value("${myblog.editors}")
    private String[] editors;

    public String getHost() {
        try {
            InetAddress ip4 = Inet4Address.getLocalHost();
            host = ip4.getHostAddress();
        } catch (UnknownHostException e) {
            host = "192.168.0.105";
            log.debug(e.getMessage());
        } finally {
            return host;
        }
    }

    public int getPort(){
        return port;
    }

    public List<String> getEditors(){
        List<String> result = Arrays.asList(editors);
        return result;
    }
}
